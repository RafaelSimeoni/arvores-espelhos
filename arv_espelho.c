#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
   char info;
   struct arvore *esq;
   struct arvore *dir;
} Arvore;


Arvore*  cria_arv_vazia (void);
Arvore*  arv_constroi (char c, Arvore* e, Arvore* d);
int      verifica_arv_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
int      arv_pertence (Arvore* a, char c);
void     arv_imprime (Arvore* a);

Arvore* cria_arv_vazia (void) {
   return NULL;
}

Arvore* arv_constroi (char c, Arvore* e, Arvore* d) {
  Arvore* a = (Arvore*)malloc(sizeof(Arvore));
  a->info = c;
  a->esq = e;
  a->dir = d;
  return a;
}

int verifica_arv_vazia (Arvore* a) {
  return (a == NULL);
}

Arvore* arv_libera (Arvore* a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera (a->esq);
    arv_libera (a->dir);
    free(a);
  }
  return NULL;
}

int arv_conta (Arvore* a){
	if(!a) return(0);

	return(1 + arv_conta(a->esq) + arv_conta(a->dir));
}

//E D R
void arv_imprime (Arvore* a){
	if(!a) return;

	arv_imprime(a->esq);
	arv_imprime(a->dir);
	printf("%c ", a->info);
}

Arvore * cria_espelho(Arvore * arv_a) {
  Arvore * arv_espelho;

  if(arv_a->esq != NULL && arv_a->dir != NULL) {
    arv_espelho = arv_constroi(arv_a->info, cria_espelho(arv_a->dir), cria_espelho(arv_a->esq));
    return arv_espelho;
  }
  else if(arv_a->esq != NULL && arv_a->dir == NULL) {
        arv_espelho = arv_constroi(arv_a->info, NULL, cria_espelho(arv_a->esq));
        return arv_espelho;
  }
  else if(arv_a->esq == NULL && arv_a->dir != NULL) {
        arv_espelho = arv_constroi(arv_a->info, cria_espelho(arv_a->dir), NULL);
        return arv_espelho;
  } else {
    arv_espelho = arv_constroi(arv_a->info, NULL, NULL);
    return arv_espelho;
  }
}

int eh_espelho(Arvore * arv_a, Arvore * arv_b) {
    if(arv_a==NULL && arv_b==NULL) return 1;
    if(arv_a==NULL || arv_b==NULL) return 0;

    return arv_a->info == arv_b->info && eh_espelho(arv_a->esq, arv_b->dir) && eh_espelho(arv_a->dir, arv_b->esq);
}



int main (int argc, char *argv[]) {
    //Construção da árvore A
    Arvore *a, *a1, *a2, *a3, *a4, *a5;
    Arvore *a_espelho;
    a1 = arv_constroi('d',cria_arv_vazia(),cria_arv_vazia());
    a2 = arv_constroi('b',cria_arv_vazia(),a1);
    a3 = arv_constroi('e',cria_arv_vazia(),cria_arv_vazia());
    a4 = arv_constroi('f',cria_arv_vazia(),cria_arv_vazia());
    a5 = arv_constroi('c',a3,a4);
    a  = arv_constroi('a',a2,a5);
    printf("Arvore: ");
    arv_imprime (a);

    printf("\n");

    //Criação da árvore espelho
    a_espelho = cria_espelho(a);

    //descomente para as árvore não serem espelhos:
    //a_espelho = arv_constroi('x',cria_arv_vazia(), arv_constroi('y',cria_arv_vazia(),cria_arv_vazia()));

    printf("Arvore espelho: ");
    arv_imprime (a_espelho);

    if(eh_espelho(a, a_espelho)) {
        printf("\nAs duas arvores sao espelhos\n");
    } else {
        printf("\nAs arvores nao sao espelhos\n");
    }

  return 0;
}
